// library
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const session = require('express-session');
const cookieParser = require('cookie-parser')
const flash = require('connect-flash');
const expressLayout = require('express-ejs-layouts');
const app = express();
const router = require('./routes/Quick-note')
require('dotenv/config');

// Set Middleware
app.set('view engine', 'ejs')
app.use(expressLayout)
app.use(express.static('public'))
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

// Konfigurasi Flash
app.use(cookieParser('secret'));
app.use(
    session({
        cookie: { maxAge: 6000},
        secret: 'secret',
        resave: true,
        saveUninitialized: true,
    })
);

app.use(flash());

app.use('/', router)


// Connect to database
mongoose.connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Error while connection to database'));
db.once('open', () => {
    console.log('Succesfully connected to database')
})

// Server
app.listen(process.env.PORT, () => {
    console.log(`Server is running on port ${process.env.PORT}`)
});