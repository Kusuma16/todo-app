const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const quickNoteSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true,
    },
    level: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('QuickNote', quickNoteSchema);