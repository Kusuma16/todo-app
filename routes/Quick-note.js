const express = require('express');
const app = express();
const router  = express.Router();
const QuickNote = require('../models/Quick-note');


// Routes
router.get('/', (req,res) => {
    res.render('Home', {
        layout: 'layouts/main-layouts',
        title: 'Halaman Home'
    })
});

router.get('/note', async(req,res) => {
    const quickNoteFind = await QuickNote.find()
    const category = ["Docker", "NodeJS", "Gitlab", "Web App"]   

    res.render('Quick-note', {
        layout: 'layouts/main-layouts',
        title: 'Quick Note',
        button: 'Add Quick Note',
        modalTitle: 'Quick Note',
        placeHolder: 'Title',
        quickNoteFind,
        category,
        expressFlash: req.flash('success')
    })
});

router.get('/lesson-plan', (req,res) => {
    res.render('Lesson-plan', {
        layout: 'layouts/main-layouts',
        title: 'Lesson Plans',
        button: 'Add Lesson Plan'
    })
});
router.get('/events', (req,res) => {
    res.render('Events', {
        layout: 'layouts/main-layouts',
        title: 'Events',
        button: 'Add an Events'
    })
});

// INSERT DATA
router.post('/add-quicknote', async(req,res) => {
    const postNote = new QuickNote({
        title: req.body.title,
        category: req.body.category,
        level: req.body.level,
        description: req.body.description
    });

    
    const saveNote = await postNote.save();
    req.flash('success', 'Note successfully added')
    res.redirect('/note')
    // try{
    //     const saveNote = await postNote.save();
    //     res.json(saveNote)
    //     res.redirect('/note')
    // }catch(err){
    //     console.log({message: err})
    // }

});



module.exports = router